DDoS, or distributed denial of service, is a type of cyberattack that tries to make a website or network resource unavailable by flooding it with malicious traffic so that it is unable to operate.

In a distributed denial-of-service (DDoS) attack, an attacker overwhelms its target with unwanted internet traffic so that normal traffic can’t reach its intended destination.


a. CC (Challenge Collapsar) attack.
Ans.
Cause:
Challenge Collapsar (CC) attack has become a real headache for defenders. However, there are many researches on DDoS attack, but few on CC attack. The related works on CC attack employ rule-based and machine learning-based models, and just validate their models on the outdated public datasets. These works appear to lag behind once the attack pattern changes. In this paper, we present a model based on packet Field Differentiated Preprocessing and Deep neural network (FDPD) to address this problem. Besides, we collected a fresh dataset which contains 7.92 million packets from real network traffic to train and validate FDPD model. The experimental results show that the accuracy of this model reaches 98.55%, the 𝐹1 value reaches 98.59%, which is 3% higher than the previous models (SVM and Random Forest-based detection model), and the training speed is increased by 17 times in the same environment. It proved that the proposed model can help defenders improve the efficiency of detecting CC attack.

Identify:
Compared with other types of DDoS attacks, a Challenge Collapsar (CC) attack is much more complicated as it has many variants making its prevention difficult. For instance, when an attack occurs, multiple requests disguised as valid HTTP requests target flawed web applications simultaneously — exhausting the web server resources. Greypanel’s AI-powered WAF can thus protect your business uptime from these CC attacks. Using real-time threat intelligence and user behavior algorithms, we can quickly distinguish between regular business traffic and malicious traffic to prevent such vicious attacks.

Effects:
In a challenge collapsar (CC) attack, the attacker uses a proxy server to generate and send disguised requests to the target host. In addition, the attacker controls other hosts in the Internet and makes them send large numbers of data packets to the target server to exhaust its resources

Mitigation:
1.Geo-restriction: We were able to restrict incoming traffic to specific regions by securing traffic from the main user base’s countries and regions, as well as preventing incoming traffic from known “attack regions” like Russia, Ukraine, and India.
2.Enabling browser-based challenge: Our web application firewall (WAF) lets us use challenge-based algorithms to filter out CC attack bots. Built on global public cloud infrastructure, we can leverage that computing power via Multi CDN to autoscale our defenses proportionate to the attack. It’s that power that let us fend off a 300-million request per minute CC attack.

b. SYN Flood attack. 
Ans. 
Cause: A TCP SYN flood DDoS attack occurs when the attacker floods the system with SYN requests in order to overwhelm the target and make it unable to respond to new real connection requests. It drives all of the target server's communications ports into a half-open state 
One of the main ways people connect to internet applications is through the Transmission Control Protocol (TCP). This connection requires a three-way handshake from a TCP service — like a web server — and involves sending a SYN (synchronization) packet from where the user connects to the server, which then returns a SYN-ACK (synchronization acknowledgement) packet, which is ultimately answered with a final ACK (acknowledgement) communication back to complete the TCP handshake.
During a SYN flood attack, a malicious client sends a large volume of SYN packets (part one of the usual handshake) but never sends the acknowledgement to complete the handshake. This leaves the server waiting for a response to these half-open TCP connections that eventually run out of capacity to accept new connections for services that track connection states. 
Identify:
What Are the Signs of a SYN Flood DDoS Attack? A SYN Flood occurs when the TCP layer is saturated, preventing the completion of the TCP three-way handshake between client and server on every port.
Effects:
SYN floods are often called “half-open” attacks because this type of DDoS attack intends to send a short burst of SYN messages into the ports, leaving insecure connections open and available, and often resulting in a complete server crash
Mitigation:
SYN floods are a form of DDoS attack that attempts to flood a system with requests in order to consume resources and ultimately disable it. You can prevent SYN flood attacks by installing an IPS, configuring your firewall, installing up to date networking equipment, and installing commercial monitoring tools.

c. SSL Attack
Ans.
Cause:
HTTPS encrypts information through the use of SSL/TLS, which acts as a digital certificate that can authenticate identities and encrypt data. SSL stripping attacks occur when a hacker intervenes in the connection between a user and a website.

Identify:
You can use a tool like SSL Checker, SSL Certificate Checker, or SSL Server Test, which will verify that an SSL certificate is installed and not expired, that the domain name is correctly listed on the certificate, and more. To use the tool, just copy and paste your site address into the search bar

Effects:
Secure Sockets Layer and, in short, it's the standard technology for keeping an internet connection secure and safeguarding any sensitive data that is being sent between two systems, preventing criminals from reading and modifying any information transferred, including potential personal details.

Mitigation:
1.Minimizing the attack surface as much as possible, by consolidating functions that use vulnerable protocols onto fewer systems, and reducing the number of systems supporting the protocols.
2.Removing or disabling use of web browsers, JavaScript, and security-impacting session cookies where they are not needed.
Restricting the number of communications using the vulnerable protocols by detecting and blocking requests to downgrade to a lesser protocol version.
3.Restricting use of the vulnerable protocols to specific entities; for example, by configuring firewalls to permit SSL/early TLS only to known IP addresses (such as business partners requiring use of the protocols), and blocking such traffic for all other IP addresses.
Enhancing detection/prevention capabilities by expanding coverage of intrusion-protection systems, updating signatures, and blocking network activity that indicates malicious behavior.
4.Actively monitoring for suspicious activity – for example, identifying unusual increases in requests for fallback to vulnerable protocols – and responding appropriately.


